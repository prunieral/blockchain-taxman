import React from "react";
import "./InsideApp.css";
import { Route, Switch } from "react-router-dom";
import Menu from "./Menu.jsx";
import PageRules from "./PageRules.jsx";
import PageWallets from "./PageWallets.jsx";
import PageOverview from "./PageOverview.jsx";
import PageReport from "./PageReport.jsx";

export default class InsideApp extends React.Component {
	constructor(props) {
		super(props);

		this.changeState = this.changeState.bind(this);

		this.state = {
			selectedMenu: "",
			wallets: [],
			rules: {},
		};
	}

	changeState(field, value) {
		this.setState({ [field]: value });
	}

	render() {
		return (
			<div id="InsideApp">
				<Menu
					selectedMenu={this.state.selectedMenu}
					changeMenu={(v) => this.changeState("selectedMenu", v)}
					disconnect={this.props.disconnect}
				/>
				<div id="InsideApp-content">
					<Switch>
						<Route path="/rules" render={(props) => <PageRules
							rules={this.state.rules}
							updateRules={(v) => this.changeState("rules", v)}
							{...props}/>}
						/>
						<Route path="/wallets" render={(props) => <PageWallets
							wallets={this.state.wallets}
							updateWallets={(v) => this.changeState("wallet", v)}
							{...props} />}
						/>
						<Route path="/overview" render={(props) => <PageOverview
							wallets={this.state.wallets}
							{...props} />}
						/>
						<Route path="/report" render={(props) => <PageReport
							wallets={this.state.wallets}
							{...props} />}
						/>

						<Route path="/" render={(props) => <PageRules {...props} />}/>
					</Switch>
				</div>
			</div>
		);
	}
}
