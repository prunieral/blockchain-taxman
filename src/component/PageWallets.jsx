import React from "react";
import "./PageWallets.css";

export default class PageWallets extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
		};
	}

	render() {
		return (
			<div id={"PageWallets"} className={"page small-max-sized-page"}>
				<div className={"row row-spaced"}>
					<div className="col-md-12">
						<h1>
							Select wallet
						</h1>

						<div>
							{this.props.wallets}
						</div>
					</div>
				</div>
			</div>
		);
	}
}
