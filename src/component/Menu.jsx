import React from "react";
import "./Menu.css";
import SideNav, {
	Toggle, Nav, NavItem, NavIcon, NavText,
} from "@trendmicro/react-sidenav";
import "@trendmicro/react-sidenav/dist/react-sidenav.css";
import { Link } from "react-router-dom";

export default class Menu extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			notifications: null,
			myCompanies: null,
		};
	}

	render() {
		return (
			<SideNav
				onSelect={(selected) => {
					console.log(selected);
					if (selected === "disconnect") {
						this.props.cookies.remove("access_token_cookie");
						window.location.replace("/");
					} else {
						this.props.changeMenu(selected);
					}
				}}
			>
				<Toggle />
				<Nav defaultSelected={this.props.selectedMenu}>
					<NavItem
						eventKey="rules"
						active={this.props.selectedMenu === "rules"}>
						<NavIcon>
							<Link to="/rules"><i className="fas fa-balance-scale" style={{ fontSize: "1.75em" }}/></Link>
						</NavIcon>
						<NavText>
							<Link to="/rules">Tax rules</Link>
						</NavText>
					</NavItem>
					<NavItem
						eventKey="wallets"
						active={this.props.selectedMenu === "wallets"}>
						<NavIcon>
							<Link to="/wallets"><i className="fas fa-wallet" style={{ fontSize: "1.75em" }}/></Link>
						</NavIcon>
						<NavText>
							<Link to="/wallets">Select wallets</Link>
						</NavText>
					</NavItem>
					<NavItem
						eventKey="overview"
						active={this.props.selectedMenu === "overview"}>
						<NavIcon>
							<Link to="/overview"><i className="fas fa-calculator" style={{ fontSize: "1.75em" }}/></Link>
						</NavIcon>
						<NavText>
							<Link to="/overview">Overview</Link>
						</NavText>
					</NavItem>
					<NavItem
						eventKey="report"
						active={this.props.selectedMenu === "report"}>
						<NavIcon>
							<Link to="/report"><i className="fas fa-file-invoice" style={{ fontSize: "1.75em" }}/></Link>
						</NavIcon>
						<NavText>
							<Link to="/report">Report</Link>
						</NavText>
					</NavItem>
				</Nav>
			</SideNav>
		);
	}
}
