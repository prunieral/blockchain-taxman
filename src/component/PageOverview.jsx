import React from "react";
import "./PageOverview.css";

export default class PageOverview extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
		};
	}

	render() {
		return (
			<div id={"PageOverview"} className={"page max-sized-page"}>
				<div className={"row row-spaced"}>
					<div className="col-md-12">
						Coming soon {this.state.undefined}
					</div>
				</div>
			</div>
		);
	}
}
